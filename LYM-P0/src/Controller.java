import java.awt.print.Printable;
import java.io.IOException;

public class Controller {



	private static Principal  manager = new Principal();




	//
	public static void leer() {

		try {
			manager.leer("src/data/test.bib");
			manager.contarTipos();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("El tamanho es:" + manager.checkearTamanho());

	}

	public static void leer2() {



		try {
			manager.leerBibTex("src/data/testbib.txt");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}


	public static void verificarEntradas() {

		manager.verificarEntradas();
		
		manager.mostrarNormales();
	}

	public static void mostrarSurplus() {
		manager.mostrarSurplus();
	}

	public static void mostrarIrregularidades() {
		manager.mostrarIrregularidades();
	}

}
