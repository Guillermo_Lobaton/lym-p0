import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;





public class Principal {


	//Constantes que representan los diferentes espacios en una publicacion
	//Articulo
	private static final String ESPACIO_AUTOR= "author";
	private static final String ESPACIO_TITULO= "title";
	private static final String ESPACIO_PERIODICO= "journal";
	private static final String ESPACIO_YEAR = "author";
	private static final String ESPACIO_VOLUMEN= "volume";
	private static final String ESPACIO_NUMERO = "number";
	private static final String ESPACIO_PAGINAS= "pages";
	private static final String ESPACIO_MES= "month";
	private static final String ESPACIO_DOI= "doi";
	private static final String ESPACIO_NOTA= "note";
	private static final String ESPACIO_LLAVE = "key";
	//Book
	private static final String ESPACIO_EDITOR= "editor";
	private static final String ESPACIO_PUBLICADOR = "publisher";
	private static final String ESPACIO_SERIE ="series";
	private static final String ESPACIO_DIRECCION= "addres";
	private static final String ESPACIO_EDICION= "edition";
	private static final String ESPACIO_URL= "url";
	//Conference and inbook
	private static final String ESPACIO_CAPITULOS= "chapter";
	//Incollection
	private static final String ESPACIO_TITULO_LIBRO= "booktitle";
	//Inproeedings
	private static final String ESPACIO_ORGANIZACION= "organization";
	//Manual
	//Master thesis
	private static final String ESPACIO_ESCUELA = "school";
	//Misc
	private static final String ESPACIO_FORMA_PUBLICACION= "howpublished";
	//Phdthesis
	//Procedings
	//techreport
	private static final String ESPACIO_INSTITUTION= "institution";
	//Unpublished

	//tabla que contiene los tipos de bibliografia
	private Hashtable<String, ArrayList<Integer>> tabla;
	private ArrayList<String> arreglo;
	private ArrayList<String> adicionales;
	private ArrayList<String> irr;
	private ArrayList<String> normales;

	public Principal() {

		//Ayuda a contar las veces que aparece un tipo de publicacion

		tabla = new Hashtable<String, ArrayList<Integer>>();
		arreglo = new ArrayList<String>();
		adicionales = new ArrayList<String>();
		irr = new ArrayList<String>();
		normales = new ArrayList<String>();

		ArrayList<Integer> art = new ArrayList<Integer>();
		ArrayList<Integer> book = new ArrayList<Integer>();
		ArrayList<Integer> booklet = new ArrayList<Integer>();
		ArrayList<Integer> conference = new ArrayList<Integer>();
		ArrayList<Integer> inbook = new ArrayList<Integer>();
		ArrayList<Integer> incollection = new ArrayList<Integer>();
		ArrayList<Integer> inproceedings = new ArrayList<Integer>();
		ArrayList<Integer> manual = new ArrayList<Integer>();
		ArrayList<Integer> masterthesis = new ArrayList<Integer>();
		ArrayList<Integer> misc = new ArrayList<Integer>();
		ArrayList<Integer> phdthesis = new ArrayList<Integer>();
		ArrayList<Integer> proceedings = new ArrayList<Integer>();
		ArrayList<Integer> techreport = new ArrayList<Integer>();
		ArrayList<Integer> unpublished= new ArrayList<Integer>();

		tabla.put("@article",art );
		tabla.put("@book",book);
		tabla.put("@booklet",booklet);
		tabla.put("@conference",conference);
		tabla.put("@inbook",inbook );
		tabla.put("@incollection",incollection );
		tabla.put("@inproceedings",inproceedings );
		tabla.put("@manual", manual );
		tabla.put("@mastersthesis",masterthesis );
		tabla.put("@miscelaneous",misc );
		tabla.put("@phdthesis",phdthesis );
		tabla.put("@proceedings",proceedings );
		tabla.put("@techreport", techreport );
		tabla.put("@unpublished",unpublished );
	}



	//Agrega una referencia a una tabla
	public void agregarAReferencia(String referencia) {


		ArrayList<Integer> arr = tabla.get(referencia);	
		arr.add(1);	
	}


	public  int checkearTamanho() {

		return tabla.get("@article").size();

	}


	public void leerBibTex(String file) throws IOException {



		//Lee todo el arhivo en un solo string
		//String contents = FileUtils.readFileToString(new File(file), "UTF-8");
		//Separa cada entrada por coma


		FileReader f = new FileReader(file);
		BufferedReader b = new BufferedReader(f);
		b.readLine();

		String sCadena;

		while((sCadena = b.readLine())!=null) {

			String cadena = "";
			if(sCadena.startsWith("@")) {

				cadena = sCadena;

				while(!(sCadena = b.readLine()).startsWith("@") ) {

					cadena+= sCadena;
				}
			}

			if(!cadena.equals("")) {
				arreglo.add(cadena);
				System.out.println(cadena);
			}
		}
	}




	public void leer(String pRuta) throws IOException {

		FileReader f = new FileReader(pRuta);
		BufferedReader b = new BufferedReader(f);
		b.readLine();
		String sCadena;
		while((sCadena = b.readLine())!=null) {

			if(sCadena.startsWith("@")) 
			{
				String[] arr = sCadena.split(Pattern.quote("{"));
				System.out.println(arr[0]);
				agregarAReferencia(arr[0]);

				System.out.println(tabla.get(arr[0]).size());

			}	
		}

		b.close();
	}


	public void contarTipos() {

		System.out.println("El numero de articulos es:"  + tabla.get("@article").size());
		System.out.println("El numero de libros es:"  + tabla.get("@book").size());
		System.out.println("El numero de booklet es:"  + tabla.get("@booklet").size());
		System.out.println("El numero de conference es:"  + tabla.get("@conference").size());
		System.out.println("El numero de incollection es:"  + tabla.get("@incollection").size());
		System.out.println("El numero de manual es:"  + tabla.get("@manual").size());
		System.out.println("El numero de mastersthesis es:"  + tabla.get("@mastersthesis").size());
		System.out.println("El numero de miscelaneous es:"  + tabla.get("@miscelaneous").size());
		System.out.println("El numero de phdthesis es:"  + tabla.get("@phdthesis").size());
		System.out.println("El numero de proceedings es:"  + tabla.get("@proceedings").size());
		System.out.println("El numero de techreport es:"  + tabla.get("@techreport").size());
		System.out.println("El numero de proceedings es:"  + tabla.get("@unpublished").size());

	}




	public void verificarEntradas() {

		//El arreglo que guarda los missing spaces
		ArrayList <String>  verificador = new ArrayList<String>();
		//El arreglo que guarda los surplus spaces
		ArrayList<String> surplus = new ArrayList<String>();
		//El arreglo de las irregularidades
		ArrayList<String> irregularidades = new ArrayList<String>();

		for (int j = 0; j < arreglo.size(); j++) {

			ArrayList temp = new ArrayList<String>();

			//Lo pasa a ArrayList
			String[] arr = arreglo.get(j).split("\\W");
			for (int i = 0; i < arr.length; i++) {
				temp.add(arr[i]);
			}

			//Examina la categoria de la entrada y reporta

			//Verificacion de los generales
			//Verificar el autor
			if(!temp.contains("Author")) {

				verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene autor.");
			}


			if(temp.contains("Key") || temp.contains("key")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene key.");
			}
			if(temp.contains("Month") || temp.contains("month")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene month.");
			}


			if(temp.contains("Organization") || temp.contains("organization")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene organization.");
			}
			if(temp.contains("Doi") || temp.contains("doi")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene doi.");
			}
			if(temp.contains("Series") || temp.contains("series")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene series.");
			}
			if(temp.contains("Edition") || temp.contains("edition")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(1) + " tiene doi.");
			}
			if(temp.contains("Addres") || temp.contains("addres")) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene doi.");
			}
			if(!temp.contains("URL") && !temp.contains("url") ) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene url.");
			}
			if(!temp.contains("Isnn") && !temp.contains("issn") ) {

				irregularidades.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene issn.");
			}
			if(!temp.contains("Type") && !temp.contains("type") ) {

				surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene issn.");
			}



			//Comeinza a evaluar caso por caso.


			//Caso articulo
			if(temp.contains("article")) {

				if(!temp.contains("Title") && !temp.contains("title")) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Journal") && !temp.contains("journal")) {
					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene journal.");
				}
				if(!temp.contains("Year") && !temp.contains("year")) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene a�o.");
				}

				if(!temp.contains("Volume") && !temp.contains("volume")) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene volumen.");
				}

				if(temp.contains("Pages") || temp.contains("pages")) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene paginas.");
				}
				if(temp.contains("Number") || temp.contains("number")) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene numero.");
				}
				if(temp.contains("Note") || temp.contains("note")) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene note	.");
				}



			}

			//Caso book
			else if(temp.contains("book")) {

				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if (!temp.contains("Editor") && !temp.contains("editor") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene editor.");
				}
				if(!temp.contains("Publisher") && !temp.contains("publisher") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene publisher.");
				}
				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
				if(!temp.contains("Volume") && !temp.contains("volume") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene volume.");
				}
				else if(!temp.contains("Number") && !temp.contains("number") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " tiene number.");
				}


			}

			//Caso booklet
			else if(temp.contains("booklet")) {

				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Howpublished") && !temp.contains("howpublished") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene howpublished.");
				}
			}

			//conference||inbook
			else if(temp.contains("conference") || temp.contains("inbook") ) {

				if (!temp.contains("Editor") && !temp.contains("editor") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene editor.");
				}
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Chapter") && !temp.contains("chapter") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}

				if(temp.contains("Pages") || temp.contains("pages")) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene paginas.");
				}
				if(!temp.contains("Publisher") && !temp.contains("publisher") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene publisher.");
				}
				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}

			}


			else if(temp.contains("incollection" )) {

				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Booktitle") && !temp.contains("booktitle") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene booktitle.");
				}
				if(!temp.contains("Publisher") && !temp.contains("publisher") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene publisher.");
				}
				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
			}

			else if(temp.contains("inproceedings" )) {
				
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Booktitle") && !temp.contains("booktitle") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene booktitle.");
				}

				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
				if(!temp.contains("Publisher") && !temp.contains("publisher") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene publisher.");
				}
			}
			
			else if(temp.contains("manual")) {
				
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				
			}
			else if(temp.contains("mastersthesis") || temp.contains("phdthesis")) {
			
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("School") && !temp.contains("school") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene school.");
				}

				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
			}

			else if(temp.contains("miscelaneous")) {
 
				if(!temp.contains("Howpublished") && !temp.contains("howpublished") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene howpublished.");
				}
				if(!temp.contains("Year") && !temp.contains("year") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene year.");
				}
			}
			
			else if(temp.contains("proeedings")) {
				
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Lisher") && !temp.contains("lisher") ) {

					surplus.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + "  tiene lisher.");
				}

				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
			}
			
			else if(temp.contains("techreport")) {
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
				if(!temp.contains("Institucion") && !temp.contains("institucion") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene school.");
				}

				if(!temp.contains("Year") && !temp.contains("year") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene year.");
				}
			}
			else if(temp.contains("unpublished")) {
				
				if(!temp.contains("Title") && !temp.contains("title") ) {

					verificador.add(" El: " + temp.get(1) + " identificado con: " + temp.get(2) + " no tiene titulo.");
				}
			}		 
		}
		
		
		adicionales = surplus;
		irr = irregularidades;
		normales = verificador;
	}

	
	
	

	public void mostrarSurplus() {
		for (int i = 0; i < adicionales.size(); i++) {
			
			System.out.println(adicionales.get(i));
		}
	}
	
	public void mostrarIrregularidades() {
		for (int i = 0; i < irr.size(); i++) {
			
			System.out.println(irr.get(i));
		}
	}
	
	public void mostrarNormales() {
		for (int i = 0; i < normales.size(); i++) {
			
			System.out.println(normales.get(i));
		}
	}
	
	
	

}